## [+] GPG-Verification+Hashsums
This repository is dedicated to automating the verification, and signing of core projects code.

[?] Such as: Debian, ParrotSec, Whonix, Qubes, HardenedBSD, SecurityOnion, OPNSense, etc.

The purpose is to accumulate the commands, urls, hashes, GPG keys, and fingerprints 

of distrobutions. This way you may replicate the process in the future, 

without searching endlessly for GPG keys & fingerprints.


## [+] Here are a copy of my videos, explaining the process:

## TailsOS - GPG Fingerprint - GPG lsign - Verify Iso 
https://www.youtube.com/watch?v=3a4CUay4TGI

## Parrot Linux - Verifying ISO Integrity With SHA512 
https://www.youtube.com/watch?v=ykGHdKWSolQ


## [?] Also checkout the LinuxDevelopers-GPG-Verification+Hashsums Branch

Which Covers: Verification of GPG signatures, and hashsums of linux project developers:

   (Mullvad, EFF, OpenVPN, ShawnWebbs, Phrack, GuardianProject, etc):

https://gitlab.com/xe1phix/GnuPG-Verification-Hashsums/tree/LinuxDevelopers-GPG-Verification-Hashsums